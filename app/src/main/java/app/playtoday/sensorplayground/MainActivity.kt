package app.playtoday.sensorplayground

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import app.playtoday.sensorplayground.sensor.*
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    private lateinit var lightSensor: LightSensor
    private lateinit var orientationSensor: OrientationSensor
    private lateinit var locationSensor: LocationSensor
    private lateinit var requestPermissionLauncher: ActivityResultLauncher<Array<String>>

    private var rationaleDisplayed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions())
        { result : Map<String,Boolean> ->
            if(result[Manifest.permission.ACCESS_FINE_LOCATION]!! && result[Manifest.permission.ACCESS_COARSE_LOCATION]!!){
                //Use feature
            } else {
                //Expliquer la dégradation
            }
        }

        val location_button = findViewById<View>(R.id.location_id)
        location_button.setOnClickListener {
            askPermissions()
        }

        lightSensor = LightSensor(this)
        val lightTextView = findViewById<TextView>(R.id.light_sensor_tv)
        lightSensor.onValueChanged { lux ->
            lightTextView.text = "Light intensity : $lux lx"
        }

        orientationSensor = OrientationSensor(this)
        val poseTextView = findViewById<TextView>(R.id.accel_sensor_tv)
        orientationSensor.onValueChanged { orientation ->
            poseTextView.text = "Yaw:${orientation.yaw}|Pitch:${orientation.pitch}|Roll:${orientation.roll}"
        }

        locationSensor = LocationSensor(this)
        val locTextView = findViewById<TextView>(R.id.gyro_sensor_tv)
        locationSensor.onValueChanged { loc ->
            locTextView.text = "Long:${loc.longitude}|Lat:${loc.latitude}|Alt:${loc.altitude}|Speed:${loc.speed}"
        }
    }

    override fun onResume() {
        super.onResume()
        lightSensor.register(this)
        orientationSensor.register(this)
        locationSensor.update()
    }

    override fun onPause() {
        super.onPause()
        lightSensor.unregister(this)
        orientationSensor.unregister(this)
    }

    private fun askPermissions(){
        when {
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED -> {
                        //Nothing to do
                    }

            (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) ||
                    shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) && !rationaleDisplayed -> {
                        //Show some UI to explain why you need the permission
                        Toast.makeText(this, "I need the location", Toast.LENGTH_LONG).show()
                        rationaleDisplayed = true
                    }

            else -> {
                //Ask for the permission
                requestPermissionLauncher.launch(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION))
                rationaleDisplayed = false
            }
        }
    }
}