package app.playtoday.sensorplayground.sensor

import android.content.Context
import android.hardware.SensorManager

data class OrientationData(
    val yaw: Float,
    val pitch: Float,
    val roll: Float,
    val accelData: AccelerometerData,
    val magnetoData: MagnetometerData
)

class OrientationSensor(context: Context) : VirtualSensor<OrientationData>() {
    private var accelerometer: AccelerometerSensor = AccelerometerSensor(context)
    private var magnetometer: MagnetometerSensor = MagnetometerSensor(context)

    private var accelData = AccelerometerData(0.0f,0.0f,0.0f)
    private var magnetoData = MagnetometerData(0.0f,0.0f,0.0f)

    private val rotationMatrix = FloatArray(9)
    private val orientationAngles = FloatArray(3)

    init{
        accelerometer.onValueChanged {
            accelData = it
            computeOrientation()
        }

        magnetometer.onValueChanged {
            magnetoData = it
            computeOrientation()
        }
    }

    override fun register(context: Context) {
        accelerometer.register(context)
        magnetometer.register(context)
    }

    override fun unregister(context: Context) {
        accelerometer.unregister(context)
        magnetometer.unregister(context)
    }

    private fun computeOrientation(){
        val accelerometerReading = FloatArray(3)
        accelerometerReading[0] = accelData.x_axis
        accelerometerReading[1] = accelData.y_axis
        accelerometerReading[2] = accelData.z_axis

        val magnetometerReading = FloatArray(3)
        magnetometerReading[0] = magnetoData.x_axis
        magnetometerReading[1] = magnetoData.y_axis
        magnetometerReading[2] = magnetoData.z_axis

        SensorManager.getRotationMatrix(
            rotationMatrix,
            null,
            accelerometerReading,
            magnetometerReading
        )
        SensorManager.getOrientation(rotationMatrix, orientationAngles)

        val data = OrientationData(
            yaw = orientationAngles[0],
            pitch = orientationAngles[1],
            roll = orientationAngles[2],
            accelData = accelData,
            magnetoData = magnetoData
        )

        fireEvent(data)
    }
}