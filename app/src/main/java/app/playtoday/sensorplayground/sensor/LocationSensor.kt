package app.playtoday.sensorplayground.sensor

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

data class LocationData(
    val longitude: Double,
    val latitude: Double,
    val altitude: Double,
    val speed: Float,
)

class LocationSensor(private val context: Context) : VirtualSensor<LocationData>() {
    private var fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

    override fun register(context: Context) {

    }

    override fun unregister(context: Context) {

    }

    fun update(){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return
        }

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
                val data = LocationData(
                    longitude = location?.longitude!!,
                    latitude = location.latitude,
                    speed = location.speed,
                    altitude = location.altitude
                )

                fireEvent(data)
            }
    }
}