package app.playtoday.sensorplayground.sensor

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorManager
import android.util.Log

class LightSensor(context: Context) : VirtualSensor<Float>(){
    private var sensorManager: SensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private var lightSensor: Sensor? = null

    private var lastValue: Float = -1.0f

    init {
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
        if(lightSensor == null){
            Log.w("LIGHT_SENSOR", "No light sensor detected")
        }
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val lux = event?.values?.get(0)

        if(lux == null || lux == lastValue) return
        lastValue = lux

        fireEvent(lastValue)
    }

    override fun register(context: Context) {
        lightSensor?.also { light ->
            sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_UI)
        }
    }

    override fun unregister(context: Context) {
        sensorManager.unregisterListener(this)
    }
}