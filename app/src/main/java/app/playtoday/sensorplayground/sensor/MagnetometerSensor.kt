package app.playtoday.sensorplayground.sensor

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorManager

data class MagnetometerData(
    var x_axis: Float,
    var y_axis: Float,
    var z_axis: Float
)

class MagnetometerSensor(context: Context) : VirtualSensor<MagnetometerData>() {

    private var magnetometer: Sensor? = null

    init {
        val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
    }

    override fun register(context: Context) {
        val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        magnetometer?.also { gyro ->
            sensorManager.registerListener(this, gyro, SensorManager.SENSOR_DELAY_GAME)
        }
    }

    override fun unregister(context: Context) {
        val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensorManager.unregisterListener(this)
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val magnetoData = MagnetometerData(
            x_axis = event?.values?.get(0)!!,
            y_axis = event.values?.get(1)!!,
            z_axis = event.values?.get(2)!!
        )

        fireEvent(magnetoData)
    }
}