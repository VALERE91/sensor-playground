package app.playtoday.sensorplayground.sensor

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorManager

data class AccelerometerData(
    var x_axis: Float,
    var y_axis: Float,
    var z_axis: Float
)

class AccelerometerSensor(context: Context) : VirtualSensor<AccelerometerData>() {

    private var accelerometer: Sensor? = null

    init {
        val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
    }

    override fun register(context: Context) {
        val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometer?.also { accel ->
            sensorManager.registerListener(this, accel, SensorManager.SENSOR_DELAY_GAME)
        }
    }

    override fun unregister(context: Context) {
        val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensorManager.unregisterListener(this)
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val accelData = AccelerometerData(
            x_axis = event?.values?.get(0)!!,
            y_axis = event.values?.get(1)!!,
            z_axis = event.values?.get(2)!!
        )

        fireEvent(accelData)
    }
}