package app.playtoday.sensorplayground.sensor

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorManager

data class GyroscopeData(
    var x_axis: Float,
    var y_axis: Float,
    var z_axis: Float
)

class GyroscopeSensor(context: Context) : VirtualSensor<GyroscopeData>() {

    private var gyroscope: Sensor? = null

    init {
        val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
    }

    override fun register(context: Context) {
        val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        gyroscope?.also { gyro ->
            sensorManager.registerListener(this, gyro, SensorManager.SENSOR_DELAY_GAME)
        }
    }

    override fun unregister(context: Context) {
        val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensorManager.unregisterListener(this)
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val gyroData = GyroscopeData(
            x_axis = event?.values?.get(0)!!,
            y_axis = event.values?.get(1)!!,
            z_axis = event.values?.get(2)!!
        )

        fireEvent(gyroData)
    }
}